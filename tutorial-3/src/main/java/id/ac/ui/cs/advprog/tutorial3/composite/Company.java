package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Company {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        this.employeesList.add(employees);
    }

    public double getNetSalaries() {
        double tmp = 0;
        for(int i=0; i<employeesList.size(); i++){
            tmp += employeesList.get(i).getSalary();
        }
        return tmp;
    }

    public List<Employees> getAllEmployees() {
        return this.employeesList;
    }
}
