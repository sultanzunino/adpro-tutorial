package id.ac.ui.cs.advprog.tutorial3.composite;

import java.awt.List;
import java.util.ArrayList;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class Main {
	public static void main(String[] args){
		Company company = new Company();
		Ceo mei = new Ceo("Mei", 500000.00);
		company.addEmployee(mei);
		
        Cto aluca = new Cto("Aluca", 320000.00);
        company.addEmployee(aluca);
	
        BackendProgrammer franky = new BackendProgrammer("Franky", 94000.00);
        company.addEmployee(franky);
		
        BackendProgrammer sonya = new BackendProgrammer("Sonya", 200000.00);
        company.addEmployee(sonya);
        
        FrontendProgrammer nami = new FrontendProgrammer("Nami",66000.00);
        company.addEmployee(nami);
		
		FrontendProgrammer natalya = new FrontendProgrammer("Natalya", 130000.00);
		company.addEmployee(natalya);
		
		UiUxDesigner budi = new UiUxDesigner("Budi", 177000.00);
		company.addEmployee(budi);
		
		NetworkExpert george = new NetworkExpert("George", 83000.00);
		company.addEmployee(george);
		
		java.util.List<Employees> anggota = company.getAllEmployees();
		System.out.println("PERUSAHAAN WUMBO");
		System.out.println("============================");
		System.out.println("Memperkerjakan:");
		for(int i=0; i<anggota.size(); i++){
			Employees pekerja = anggota.get(i);
			System.out.println(pekerja.getName() + " sebagai " + pekerja.getRole() + " dengan gaji $" + pekerja.salary );
		}
		System.out.println();
		System.out.println("Total Gaji Seluruh Karyawan Perusahaan : $" + company.getNetSalaries());
	}
}
