package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Main {
	public static void main(String[] args){
		System.out.println();
		Food thickBurgerNormalCheese = BreadProducer.THICK_BUN.createBreadToBeFilled();
		System.out.println("Nino order a Special " + thickBurgerNormalCheese.getDescription() + " with Cheese");
		thickBurgerNormalCheese = FillingDecorator.LETTUCE.addFillingToBread(thickBurgerNormalCheese);
		thickBurgerNormalCheese = FillingDecorator.TOMATO.addFillingToBread(thickBurgerNormalCheese);
		thickBurgerNormalCheese = FillingDecorator.CUCUMBER.addFillingToBread(thickBurgerNormalCheese);
		thickBurgerNormalCheese = FillingDecorator.BEEF_MEAT.addFillingToBread(thickBurgerNormalCheese);
		thickBurgerNormalCheese = FillingDecorator.CHEESE.addFillingToBread(thickBurgerNormalCheese);
		thickBurgerNormalCheese = FillingDecorator.TOMATO_SAUCE.addFillingToBread(thickBurgerNormalCheese);
		System.out.println(thickBurgerNormalCheese.getDescription());
		System.out.println();
		System.out.println("The Price is $" + thickBurgerNormalCheese.cost());
	}
}